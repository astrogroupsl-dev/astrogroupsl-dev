# Hi there 👋, I'm Your Name 😂. No it's Nadeesha..😉

I'm a Frontend Developer from Sri Lanka.
![Profile views](https://komarev.com/ghpvc/?username=astrogroupsl-dev&label=Profile%20views&color=0e75b6&style=flat)

## 🏆 GitHub Trophies

<img src='https://github-profile-trophy.vercel.app/?username=astrogroupsl-dev' alt='astrogroupsl-dev'>

## 🔗 Connect with me

<a href="https://www.facebook.com/profile.php?id=100072737604608&mibextid=ZbWKwL" target="_blank"><img src="https://img.shields.io/badge/Facebook-%231877F2.svg?&style=flat-square&logo=facebook&logoColor=white" alt="Facebook"></a>
<a href="https://www.instagram.com/nadeesha_igneshius/" target="_blank"><img src="https://img.shields.io/badge/Instagram-%23E4405F.svg?&style=flat-square&logo=instagram&logoColor=white" alt="Instagram"></a>
<a href="https://www.linkedin.com/in/nadeesha-fernando-7980141bb" target="_blank"><img src="https://img.shields.io/badge/LinkedIn-%230077B5.svg?&style=flat-square&logo=linkedin&logoColor=white" alt="LinkedIn"></a>
<a href="https://open.spotify.com/user/31daycwk5datuv5g7nyr4lo3fmtu?si=2dba23d38b8e4711" target="_blank"><img src="https://img.shields.io/badge/Spotify-%231ED760.svg?&style=flat-square&logo=spotify&logoColor=white" alt="Spotify"></a>
<a href="https://twitter.com/Nadeesh78909401" target="_blank"><img src='https://img.shields.io/twitter/url?url=https%3A%2F%2Ftwitter.com%2FNadeesh78909401' alt='Twitter'></a>

## 📊 GitHub Stats

<img src='https://github-readme-stats.vercel.app/api/top-langs?username=astrogroupsl-dev&show_icons=true&locale=en&layout=compact' alt='astrogroupsl-dev'>
<img src='https://github-readme-stats.vercel.app/api?username=astrogroupsl-dev&show_icons=true&locale=en' alt='astrogroupsl-dev'>
<img src='https://github-readme-streak-stats.herokuapp.com/?user=astrogroupsl-dev&' alt='astrogroupsl-dev'>

## 📈 Contribution Graph

![Contribution](https://github-readme-activity-graph.vercel.app/graph?username=astrogroupsl-dev&bg_color=fffff0&color=708090&line=24292e&point=24292e&area=true&hide_border=true)

## 📚 GitHub All Stats

<img src='https://myreadme.vercel.app/api/embed/astrogroupsl-dev?panels=userstatistics,toprepositories,toplanguages,commitgraph' alt='astrogroupsl-dev'>
